import PageContent from "./pageContent";
import {getEmployees} from "../axios/employee";
import Employee from "./employee";
import asyncErrorDecorator from "../utils/asyncErrorDecorator";
import LoadingMessage from "../message/messagesToSend/LoadingMessage";
import destination from "../constants/destination";

const pageContent = new PageContent();
const pageInit = asyncErrorDecorator(
  async function pageInit(pageContent) {
    try {
      await pageContent.init();
    } catch (error) {
      throw error;
    }
  }
);

async function initPageScript(startLoading, stopLoading) {
  if (startLoading === undefined) {
    startLoading = new LoadingMessage(destination.extensionScript, {isLoading: true}).sendMessage;
  }
  if (stopLoading === undefined) {
    stopLoading = new LoadingMessage(destination.extensionScript, {isLoading: false}).sendMessage;
  }

  startLoading();
  try {
    await pageInit(pageContent);
    let employees = await getEmployees()
    employees = employees.map(employee => new Employee(employee));
    pageContent.fillEmployeesInfo(employees);
    stopLoading();
  } catch (error) {
    stopLoading();
  }
}

function startThePageContentUpdateCycle() {
  setInterval(() => {
    const isCurrentUrlCorrect = pageContent.checkIsCurrentUrlCorrect(true);
    if (isCurrentUrlCorrect && !pageContent.isInitSuccess) {
      initPageScript();
    } else if (!isCurrentUrlCorrect) {
      pageContent.isInitSuccess = false;
    }
  }, 7000);
}

export {startThePageContentUpdateCycle};
export default initPageScript;



