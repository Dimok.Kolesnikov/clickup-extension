class ContentSideError {
  constructor({id, type, message, errorNumber}) {
    this.id = id;
    this.type = type;
    this.message = message;
    this.errorNumber = errorNumber;
  }
}

export default ContentSideError;