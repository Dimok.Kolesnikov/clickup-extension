class Employee {
  _firstName = '';
  _lastName = '';
  _middleName = '';
  _department = '';
  _level = '';
  _timePerWeek = '';

  set lastName(value) {
    if (typeof value === 'string' && !!value) {
      this._lastName = value;
    } else {
      throw new Error(`Wrong last name: ${value}`);
    }
  }

  get lastName() {
    return this._lastName;
  }

  set middleName(value) {
    if (typeof value === 'string' || !value) {
      this._middleName = value || "";
    } else {
      throw new Error(`Wrong middle name: ${value}`);
    }
  }

  get middleName() {
    return this._middleName;
  }

  set firstName(value) {
    if (typeof value === 'string' && !!value) {
      this._firstName = value;
    } else {
      throw new Error(`Wrong first name: ${value}`);
    }
  }

  get firstName() {
    return this._firstName;
  }

  set department(value) {
    if (typeof value === 'string' && !!value) {
      this._department = value;
    } else {
      throw new Error(`Wrong department: ${value}`);
    }
  }

  get department() {
    return this._department;
  }

  set level(value) {
    if (typeof value === 'string' && !!value) {
      this._level = value;
    } else {
      throw new Error(`Wrong level: ${value}`);
    }
  }

  get level() {
    return this._level;
  }

  set timePerWeek(value) {
    if (typeof value === "number" && !!value) {
      this._timePerWeek = value;
    } else {
      throw new Error(`Wrong time per week: ${value}`);
    }
  }

  get timePerWeek() {
    return this._timePerWeek;
  }


  constructor({firstName, middleName, lastName, department, level, timePerWeek}) {
    this._firstName = firstName;
    this._middleName = middleName;
    this._lastName = lastName;
    this._department = department;
    this._level = level;
    this._timePerWeek = timePerWeek;
  }

  getMatchingEmployee(fullName) {
    try {
      const splittedFullName = fullName.split(' ');
      let numberOfMatches = 0;
      for (let fullNamePart of splittedFullName) {
        if (fullNamePart === this.firstName) {
          numberOfMatches += 1;
        } else if (fullNamePart === this.middleName) {
          numberOfMatches += 1;
        } else if (fullNamePart === this.lastName) {
          numberOfMatches += 1;
        }
      }
      return numberOfMatches === splittedFullName.length;
    } catch (error) {
      throw error;
    }
  }
}

export default Employee;