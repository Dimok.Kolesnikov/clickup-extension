import initPageScript, {startThePageContentUpdateCycle} from "./initPageScript";
import MessageListener from "./MessageListener";

window.onload = async () => {
  const messageListener = new MessageListener();
  messageListener.startListen();
  await initPageScript();
  messageListener.setIsAllowedOnlyExtensionOpenMessageType(false);
  startThePageContentUpdateCycle();
}