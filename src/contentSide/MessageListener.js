import messageTypes from "../constants/messageTypes";
import BaseMessage from "../message/messagesToSend/BaseMessage";
import destination from "../constants/destination";
import requestTypes from "../constants/requestTypes";
import initPageScript from "./initPageScript";
import {importCSV, importCSVExample, exportCSV} from "../axios/employee";
import LoadingMessage from "../message/messagesToSend/LoadingMessage";
import ErrorMessage from "../message/messagesToSend/ErrorMessage";
import RequestMessage from "../message/messagesToSend/RequestMessage";

class MessageListener {
  constructor(isAllowedOnlyExtensionOpenMessageType) {
    this.isAllowedOnlyExtensionOpenMessageType = isAllowedOnlyExtensionOpenMessageType || true;
    this._listenerHandler = this._listenerHandler.bind(this);
  }

  startListen() {
    chrome.runtime.onMessage.addListener(this._listenerHandler);
  }

  setIsAllowedOnlyExtensionOpenMessageType(value) {
    this.isAllowedOnlyExtensionOpenMessageType = value;
  }

  _listenerHandler(request, sender, sendResponse) {
    try {
      if (this.isAllowedOnlyExtensionOpenMessageType && request.type !== messageTypes.extensionOpen) {
        throw new Error(`Only ${messageTypes.extensionOpen} message type is Allowed`);
      }
      switch (request.type) {
        case messageTypes.extensionOpen:
          this._extensionOpen(request, sender, sendResponse);
          break;
        case messageTypes.error:
          this._errorsHandler(request, sender, sendResponse);
          break;
        case messageTypes.request:
          this._requestHandler(request, sender, sendResponse);
          break;
        case messageTypes.refresh:
          this._refreshHandler(request, sender, sendResponse);
          break;
        default:
          console.error('Unknown request', request);
      }
    } catch (e) {
      console.error(e);
    }
  }

  _extensionOpen(request, sender, sendResponse) {
    const baseMessage = new BaseMessage(destination.extensionScript);
    sendResponse(true);
    baseMessage.sendMessagesFromStacks();
  }

  _errorsHandler(request, sender, sendResponse) {
    new ErrorMessage(destination.extensionScript, {errors: request.errors, action: request.action}).sendMessage();
  }

  async _requestHandler(request, sender, sendResponse) {
    const {requestType} = request;
    switch (requestType) {
      case requestTypes.importCSV:
        try {
          const url = await importCSV();
          const a = document.createElement('a');
          a.href = url;
          a.download;
          a.click();
        } catch (e) {
          console.error('Failed to download file');
        }
        break;
      case requestTypes.importCSVExample:
        try {
          const url = await importCSVExample();
          const a = document.createElement('a');
          a.href = url
          a.download;
          a.click();
        } catch (e) {
          console.error('Failed to download file');
        }
        break;
      case requestTypes.exportCSV:
        const inputFile = document.createElement('input');
        inputFile.type = 'file';
        inputFile.accept = 'text/csv';
        inputFile.click();
        inputFile.onchange = async event => {
          const file = event.target.files[0];
          await exportCSV(file);
          initPageScript();
        };
        break;
      default:
        console.error('Unknown request', requestType);
    }
  }

  _refreshHandler(request, sender, sendResponse) {
    const startLoading = new LoadingMessage(destination.extensionScript, {isLoading: true, isRefresh: true});
    const stopLoading = new LoadingMessage(destination.extensionScript, {isLoading: false, isRefresh: true});
    initPageScript(startLoading.sendMessage, stopLoading.sendMessage);
  }
}

export default MessageListener;