import LoadingStack from "./LoadingStack";
import ErrorStack from "./ErrorStack";
import messageTypes from "../../constants/messageTypes";

class MessageStackController {
  constructor() {
    this.stacks = {
      loadingStack: new LoadingStack(),
      errorStack: new ErrorStack(),
    };
    this.returnedStacks = [];
  }

  pushToStack(messageObject) {
    const {type} = messageObject;
    let message;
    switch (type) {
      case messageTypes.loading:
        message = this.stacks.loadingStack.push(messageObject);
        return this._returnMessages({stackName: this.stacks.loadingStack.name, message})
      case messageTypes.error:
        message = this.stacks.errorStack.push(messageObject);
        return this._returnMessages({stackName: this.stacks.errorStack.name, message});
      default:
        console.error(`Type ${type} is unknown`);
        return null;
    }
  };

  getMessagesToSend() {
    const stackMessages = [];
    Object.values(this.stacks).forEach(stack => {
      const message = stack.getMessagesToSend();
      if (message) {
        stackMessages.push({stackName: stack.name, message});
      }
    });
    return this._returnMessages(stackMessages);
  };


  messagesSent() {
    this.returnedStacks.forEach(stackName => {
      if (this.stacks[stackName].postDispatchAction) {
        this.stacks[stackName].postDispatchAction();
      }
    });
    this._removeReturnedStacks();
  }

  _returnMessages (stacksMessages) {
    if (Array.isArray(stacksMessages)) {
      const messages = [];
      stacksMessages.forEach((stackMessage) => {
        const {stackName, message} = stackMessage;
        this.returnedStacks.push(stackName);
        messages.push(message);
      });
      return messages;
    } else {
      const {stackName, message} = stacksMessages;
      this.returnedStacks.push(stackName);
      return message;
    }
  }

  _removeReturnedStacks(){
    this.returnedStacks = [];
  }
}

export default new MessageStackController();