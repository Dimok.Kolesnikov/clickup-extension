class LoadingStack {
  constructor() {
    this.name = name || 'loadingStack';
    this.stack = [];
    this.length = 0;
    this.lastElement = null;
  }

  push(message) {
    const {isLoading} = message;
    if (isLoading) {
      if (!this.lastElement?.isLoading){
        this._push(message);
      }
    } else {
      if (this.lastElement?.isLoading) {
        this._pop();
        this._push(message);
      }
    }

    return this._returnStack();
  }

  getMessagesToSend() {
    return this._returnStack();
  }

  cleaningTrash() {
    if (this.length && !this.stack[0].isLoading){
      this.deleteAll();
    }
  }

  deleteAll() {
    this.stack = [];
    this.length = 0;
    this.lastElement = null;
  }

  postDispatchAction(){
    this.cleaningTrash();
  }

  _push(messageObject) {
    this.stack.push(messageObject);
    this._setStackParams();
  }

  _pop() {
    this.stack.pop();
    this._setStackParams();
  }

  _setStackParams() {
    this.length = this.stack.length;
    this.lastElement = this.length > 0
      ? this.stack[this.length - 1]
      : null;
  }

  _returnStack() {
    return this.length ? this.stack[0] : null;
  }
}

export default LoadingStack;