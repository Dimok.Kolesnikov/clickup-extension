import messageTypes from "../../constants/messageTypes";

class ErrorStack {
  constructor(name) {
    this.name = name || 'errorStack';
    this.stack = [];
    this.length = 0;
  }

  push(messageObject) {
    const {errors, action} = messageObject;
    if (action === 'add') {
      this._push(errors);
    } else if (action === 'delete') {
      this._pop(errors);
    }
    return this._returnStack();
  }

  getMessagesToSend() {
    return this._returnStack();
  }

  deleteOne(id) {
    const index = this.stack.findIndex(item => item.id === id);
    this.stack.splice(index, 1);
    this._setStackParams();
  }

  deleteAll() {
    this.stack = [];
    this._setStackParams();
  }

  _push(errors) {
    errors.forEach(error => {
      const existErrorIndex = this.stack.findIndex(item => item.id === error.id);
      if (existErrorIndex === -1) {
        this.stack.unshift(error);
      }
    });
    this._setStackParams();
  }

  _pop(errors) {
    if (errors === 'all') {
      this.deleteAll();
    } else {
      errors.forEach(error => {
        const {id} = error;
        this.deleteOne(id);
      });
    }
  }

  _setStackParams() {
    this.length = this.stack.length;
  }

  _returnStack() {
    if (this.length) {
      return {
        type: messageTypes.error,
        errors: this.stack.map(error => ({...error})),
      }
    } else {
      return {
        type: 'error',
        errors: null,
      };
    }
  }
}

export default ErrorStack;