import {colors, fields} from "../constants/fields";
import ContentSideError from "./ContentSideError";
import errorTypes from "../constants/errorTypes";
import matchingUrls from "../constants/matchingUrls";

class PageContent {
  constructor() {
    this.isInitSuccess = false;
    this.tabs = [];
    this.employees = [];
    this.isDesiredTabActive = false;
  }

  async init(desiredTabName = 'Timeline') {
    try {
      this.checkIsCurrentUrlCorrect(false);
      await this.findTabs(desiredTabName);
      await this.checkIsDesiredTabActive(desiredTabName, false);
      await this.findEmployees();
      this.isInitSuccess = true;
    } catch (error) {
      this.isInitSuccess = false;
      throw error;
    }
  }

  async findTabs(desiredTabName) {
    const method = document.getElementsByTagName.bind(document);
    const qualifiedName = 'cu-data-view-item';
    try {
      const tabs = await this._findElements(method, qualifiedName);
      const newTabs = [];
      for (let i = 0; i < tabs.length; i += 1) {
        newTabs.push({containerElement: tabs[i], name: tabs[i].textContent.trim()});
      }
      this.tabs = newTabs;
      return newTabs;
    } catch (e) {
      throw new ContentSideError({
        id: 'findTabs',
        type: errorTypes.pageContent.name,
        message: `Не удалось найти таб ${desiredTabName} на странице`
      });
    }
  }

  async findEmployees() {
    const method = document.getElementsByClassName.bind(document);
    const qualifiedName = 'cu-timeline-group-row__group-details-name';
    const qualifiedNameContainer = 'cu-timeline-group-row__group-details ng-star-inserted';
    try {
      const employees = await this._findElements(method, qualifiedName);
      const newEmployees = [];
      for (let i = 0; i < employees.length; i += 1) {
        const newEmployeeInfo = {
          containerElement: employees[i].parentNode,
          fullNameContainer: employees[i],
          fullName: employees[i].textContent.trim()
        };
        const existingEmployeeIndex = this.employees.findIndex(employee => {
          return employee.containerElement.isSameNode(newEmployeeInfo.containerElement);
        });
        if (existingEmployeeIndex !== -1) {
          newEmployees.push({...this.employees[existingEmployeeIndex]});
        } else {
          newEmployees.push(newEmployeeInfo);
        }
      }
      this.employees = newEmployees;
      return newEmployees;
    } catch (e) {
      throw new ContentSideError({
        id: 'findEmployees',
        type: errorTypes.pageContent.name,
        message: 'Не удалось найти список сотрудников на странице'
      });
    }
  }

  async _findElements(findMethod, qualifiedName) {
    let elements = [];
    let counter = 0;
    const frequency = 500;//ms
    const maxRepetitionsNumber = 20;
    return new Promise((resolve, reject) => {
      const intervalId = setInterval(() => {
        counter += 1;
        if (counter >= maxRepetitionsNumber) {
          clearInterval(intervalId);
          reject();
        }
        elements = findMethod(qualifiedName);
        if (elements.length) {
          clearInterval(intervalId);
          resolve(elements);
        }
      }, frequency);
    });
  }

  checkIsCurrentUrlCorrect(isReturnValueNeeded) {
    const currentUrl = window.location.href;
    const isCurrentUrlCorrect = matchingUrls.some(url => url.test(currentUrl));
    if (!isCurrentUrlCorrect) {
      if (isReturnValueNeeded) {
        return false;
      } else {
        throw new ContentSideError({
          id: 'currentUrl',
          type: errorTypes.pageContent.name,
          message: 'Текущая страница не поддерживается расширением',
        });
      }
    }
    return true;
  }

  async checkIsDesiredTabActive(tabName = 'Timeline', isUpdate) {
    if (isUpdate) {
      await this.findTabs();
    }
    for (let tab of this.tabs) {
      const {name, containerElement} = tab;
      if (name === tabName && containerElement.classList.contains('cu-data-view-item_selected')) {
        this.isDesiredTabActive = true;
        return true;
      }
    }
    this.isDesiredTabActive = false;
    throw new ContentSideError({
      id: 'init',
      type: errorTypes.pageContent.name,
      message: `Таб "${tabName}" не является выбранным`
    });
  }

  fillEmployeesInfo(responseEmployees) {
    this.employees = this.employees.reduce((accumulator, employee) => {
      for (let responseEmployee of responseEmployees) {
        if (responseEmployee.getMatchingEmployee(employee.fullName)) {
          const fieldsContainer = this._printDetailedInfo(employee, responseEmployee);
          accumulator.push({...employee, fieldsContainer, detailedInfo: responseEmployee});
          return accumulator;
        }
      }
      if (employee.fieldsContainer) {
        employee.fieldsContainer.remove();
        delete employee.fieldsContainer;
      }
      accumulator.push(employee);
      return accumulator;
    }, []);
  }

  _printDetailedInfo(employee, detailedInfo) {
    const {containerElement, fieldsContainer} = employee;
    const {department, level, timePerWeek} = detailedInfo;
    if (!fieldsContainer) {
      const newFieldsContainer = addDetailedInfoContainer(this._createFieldsContainer());
      containerElement.appendChild(newFieldsContainer);
      return newFieldsContainer;
    } else {
      return addDetailedInfoContainer(fieldsContainer);
    }

    function addDetailedInfoContainer(fieldsContainer) {
      fieldsContainer.innerHTML = null;
      fieldsContainer.appendChild(addInfoCard(fields.department.name, department));
      fieldsContainer.appendChild(addInfoCard(fields.level.name, level));
      fieldsContainer.appendChild(addInfoCard(fields.timePerWeek.name, timePerWeek));
      return fieldsContainer;
    }

    function addInfoCard(cardType, value) {
      const infoCard = document.createElement('div');
      infoCard.classList.add('field');
      infoCard.innerText = value;
      infoCard.style.backgroundColor = fields[cardType][value]?.color ?? colors.default;

      return infoCard;
    }
  }

  _createFieldsContainer() {
    const fieldsContainer = document.createElement('div');
    fieldsContainer.classList.add('fieldsContainer');
    return fieldsContainer;
  }
}

export default PageContent;