import ErrorMessage from "../message/messagesToSend/ErrorMessage";
import destination from "../constants/destination";

function asyncErrorDecorator(asyncMethod) {
  let previousErrorMessage = null;

  function deletePreviousError(currentError) {
    if (previousErrorMessage) {
      if (!currentError || currentError.id !== previousErrorMessage.messageObject.errors[0].id) {
        previousErrorMessage.messageObject.action = 'delete';
        previousErrorMessage.sendMessage();
        previousErrorMessage = null;
      }
    }
  }

  function sendError(error) {
    const errorMessage = new ErrorMessage(destination.extensionScript, {
      errors: [error],
      action: 'add',
    });
    errorMessage.sendMessage();
    return errorMessage;
  }

  return async function () {
    try {
      const response = await asyncMethod(...arguments);
      deletePreviousError();
      return response;
    } catch (error) {
      deletePreviousError(error);
      previousErrorMessage = sendError(error);
      throw error;
    }
  };
}

export default asyncErrorDecorator;