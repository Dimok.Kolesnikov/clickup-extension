import ContentSideError from "../contentSide/ContentSideError";
import errorTypes from "../constants/errorTypes";
import requestTypes from "../constants/requestTypes";

export default function (error, methodName, message) {
  const requestStatus = error?.response?.status;
  switch (requestStatus) {
    case 500:
      return errorHandler500(error.response, methodName, message);
    case 422:
      return errorHandler422(error.response, methodName, message);
    default:
      return errorHandler500({status: 500}, methodName, message);
  }
}

function errorHandler500(error, methodName, message) {
  return new ContentSideError({
    id: `${methodName}${error.status}`,
    type: errorTypes.request.name,
    message,
  });
}

function errorHandler422(error, methodName) {
  const messages = [];
  error.data.data.forEach(validationError => {
    Object.values(validationError).forEach(value => {
      messages.push({text: value.message, errorNumber: value.rowNumber});
    });
  });
  return new ContentSideError({
    id: `${methodName}${error.status}`,
    type: errorTypes.csvParsing.name,
    message: messages,
  });
}