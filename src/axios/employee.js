import axiosInstance from "./index";
import apiList from "../constants/apiList";
import LoadingMessage from "../message/messagesToSend/LoadingMessage";
import destination from "../constants/destination";
import backendErrorHandler from "../utils/backendErrorHandler";
import asyncErrorDecorator from "../utils/asyncErrorDecorator";

const startLoading = new LoadingMessage(destination.extensionScript, {isLoading: true});
const stopLoading = new LoadingMessage(destination.extensionScript, {isLoading: false});

async function getEmployees() {
  try {
    const respose = await axiosInstance({
      method: 'get',
      url: apiList.employees,
    });
    return respose.data.data.items;
  } catch (e) {
    throw backendErrorHandler(e, 'getEmployees', 'Не удалось загрузить список сотрудников с сервера');
  }
}

async function importCSV() {
  startLoading.sendMessage();
  try {
    const response = await axiosInstance({
      method: 'get',
      url: apiList.employeesCSV,
    });
    stopLoading.sendMessage();
    return response.data.data.url;
  } catch (e) {
    stopLoading.sendMessage();
    throw backendErrorHandler(e, 'importCSV', 'Не удалось скачать список сотрудников');
  }
}

async function importCSVExample() {
  startLoading.sendMessage();
  try {
    const response = await axiosInstance({
      method: 'get',
      url: apiList.employeesCSVExample,
    });
    stopLoading.sendMessage();
    return response.data.data.url;
  } catch (e) {
    stopLoading.sendMessage();
    throw backendErrorHandler(e, 'importCSVExample', 'Не удалось скачать пример списка сотрудников');
  }
}

async function exportCSV(employeesFile) {
  startLoading.sendMessage();
  const formData = new FormData();
  formData.append('employeesFile', employeesFile);
  try {
    await axiosInstance({
      method: 'post',
      url: apiList.employeesCSV,
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    stopLoading.sendMessage();
  } catch (e) {
    stopLoading.sendMessage();
    throw backendErrorHandler(e, 'exportCSV', 'Не удалось загрузить список сотрудников');
  }
}

getEmployees = asyncErrorDecorator(getEmployees);
importCSV = asyncErrorDecorator(importCSV);
importCSVExample = asyncErrorDecorator(importCSVExample);
exportCSV = asyncErrorDecorator(exportCSV);

export {getEmployees, importCSV, importCSVExample, exportCSV};