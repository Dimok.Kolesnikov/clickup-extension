import BaseMessage from "./BaseMessage";
import messageTypes from "../../constants/messageTypes";

class LoadingMessage extends BaseMessage{
  constructor(destination, messageObject) {
    super(destination);
    this.messageObject = null;
    this._initMessageObject(messageObject);
    this.sendMessage = this.sendMessage.bind(this);
  }

  _validateMessageObject(messageObject){
    const errorsArray = [];
    if (Object.getPrototypeOf(messageObject) === Object.prototype) {
      const {message} = messageObject;
      if (!(typeof message === "string" || message === undefined)) {
        errorsArray.push('message field is incorrect');
      }
    } else {
      errorsArray.push('messageObject is not an object');
    }

    return errorsArray;
  }

  _initMessageObject(messageObject) {
    const errors = this._validateMessageObject(messageObject);
    if (!errors.length) {
      this.messageObject = {
        type: messageTypes.loading,
        ...messageObject,
        isLoading: !!messageObject.isLoading,
      }
    } else {
      throw new Error(errors.join('; '));
    }
  }

  sendMessage() {
    super._sendMessage(this.messageObject);
  }
}

export default LoadingMessage;