import destination from "../../constants/destination";
import messageStackController from "../../contentSide/messageStack/MessageStackController";
import messageTypes from "../../constants/messageTypes";

class BaseMessage {
  constructor(destination, domController) {
    this.destination = destination;
    this.domController = domController;
  }

  _sendMessage(messageObject) {
    switch (this.destination) {
      case destination.contentScript:
        this._toContentScript(messageObject);
        break;
      case destination.extensionScript:
        this._toExtensionScript(messageObject);
        break;
      default:
        throw new Error(`Destination ${this.destination} is unknown`);
    }
  }

  extensionOpen() {
    if (this.destination === destination.contentScript) {
      this._sendMessage({type: messageTypes.extensionOpen});
    } else {
      throw new Error('ExtensionOpen message has to only be sent to contentScript');
    }
  }

  sendMessagesFromStacks(){
    const messages = messageStackController.getMessagesToSend();
    messages.forEach(message => this._toExtensionScript(null, message));
  }

  _toContentScript(messageObject) {
    chrome.tabs.query({active: true, currentWindow: true}, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, messageObject, response => {
        if (!response) {
          this.domController.disableButtons(true);
        }
      });
    });
  }

  _toExtensionScript(messageObject, messageFromStack) {
    const message = messageFromStack ? messageFromStack : messageStackController.pushToStack(messageObject);
    chrome.runtime.sendMessage(message);
    messageStackController.messagesSent();
  }
}

export default BaseMessage;