import BaseMessage from "./BaseMessage";
import messageTypes from "../../constants/messageTypes";

class RefreshMessage extends BaseMessage {
  constructor(destination, messageObject) {
    super(destination);
    this.messageObject = null;
    this._initMessageObject(messageObject);
    this.sendMessage = this.sendMessage.bind(this);
  }

  _initMessageObject() {
    this.messageObject = {
      type: messageTypes.refresh,
    }
  }

  sendMessage() {
    super._sendMessage(this.messageObject);
  }
}

export default RefreshMessage;