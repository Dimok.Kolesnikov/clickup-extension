import BaseMessage from "./BaseMessage";
import requestTypes from "../../constants/requestTypes";
import messageTypes from "../../constants/messageTypes";

class RequestMessage extends BaseMessage{
  constructor(destination, messageObject) {
    super(destination);
    this.messageObject = null;
    this._initMessageObject(messageObject);
    this.sendMessage = this.sendMessage.bind(this);
  }

  _validateMessageObject(messageObject){
    const errorsArray = [];

    if (Object.getPrototypeOf(messageObject) === Object.prototype) {
      const {requestType} = messageObject;
      const {importCSV, exportCSV, importCSVExample} = requestTypes;
      if (requestType !== importCSV && requestType !== exportCSV && requestType !== importCSVExample) {
        errorsArray.push(`requestType ${requestType} is incorrect`);
      }
    } else {
      errorsArray.push('messageObject is not an object');
    }

    return errorsArray;
  }

  _initMessageObject(messageObject) {
    const errors = this._validateMessageObject(messageObject);
    if (!errors.length) {
      this.messageObject = {
        type: messageTypes.request,
        ...messageObject,
      }
    } else {
      throw new Error(errors.join('; '));
    }
  }

  sendMessage() {
    super._sendMessage(this.messageObject);
  }
}

export default RequestMessage;