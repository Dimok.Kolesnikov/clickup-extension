import BaseMessage from "./BaseMessage";
import messageTypes from "../../constants/messageTypes";

class ErrorMessage extends BaseMessage{
  constructor(destination, messageObject) {
    super(destination);
    this.messageObject = null;
    this.sendMessage = this.sendMessage.bind(this);
    this._initMessageObject(messageObject);
  }

  _validateMessageObject(messageObject){
    const errorsArray = [];

    function errorsValidate(errors) {
      if (Array.isArray(errors)) {
        const isIncorrectArray = errors.some(error => error.type === undefined);
        if (isIncorrectArray) {
          errorsArray.push('Errors field is incorrect');
        }
      } else if (typeof errors === "string") {
        if (errors !== 'all') {
          errorsArray.push('Errors field is incorrect');
        }
      } else {
        errorsArray.push('Errors field type is incorrect');
      }
    }

    function actionValidate(action) {
      if (typeof action === 'string') {
        if (action !== 'add' && action !== 'delete') {
          errorsArray.push('Action field is incorrect');
        }
      } else {
        errorsArray.push('Action field type is not string');
      }
    }


    if (messageObject && Object.getPrototypeOf(messageObject) === Object.prototype) {
      const {errors, action} = messageObject;
      errorsValidate(errors);
      actionValidate(action);
    } else {
      errorsArray.push('messageObject is not an object');
    }

    return errorsArray;
  }

  _initMessageObject(messageObject) {
    const errors = this._validateMessageObject(messageObject);
    if (!errors.length) {
      this.messageObject = {
        type: messageTypes.error,
        ...messageObject,
      }
    } else {
      throw new Error(errors.join('; '));
    }
  }

  sendMessage() {
    super._sendMessage(this.messageObject);
  }
}

export default ErrorMessage;