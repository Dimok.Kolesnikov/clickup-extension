export default {
  pageContent: {
    name: 'pageContent',
    title: 'Ошибки страницы',
    errorTitle: 'Ошибка',
    priority: 3,
  },
  request: {
    name: 'request',
    title: 'Ошибки запросов',
    errorTitle: 'Ошибка',
    priority: 2,
  },
  csvParsing: {
    name: 'csvParsing',
    title: 'Ошибки парсинга csv',
    errorTitle: 'Строка',
    priority: 1,
  }
}