export default {
  employees: '/employees',
  employeesCSV: '/employees/csv',
  employeesCSVExample: '/employees/csv-example',
}