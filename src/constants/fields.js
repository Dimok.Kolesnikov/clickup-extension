const colors = {
  default: '#dbdbdb',
  green: '#6ae66c',
  red: '#e6876a',
};

const fields = {
  department: {
    name: 'department',
    frontend: {
      name: 'frontend',
      displayName: 'Frontend',
      color: colors.red,
    },
    backend: {
      name: 'backend',
      displayName: 'Backend',
      color: colors.green,
    },
  },
  level: {
    name: 'level',
    junior: {
      name: 'junior',
      displayName: 'Junior',
    },
    middle: {
      name: 'middle',
      displayName: 'Middle',
    },
    senior: {
      name: 'senior',
      displayName: 'Senior',
    },
  },
  timePerWeek: {
    name: 'timePerWeek',
    '20': {
      name: 'twentyHours',
      displayName: 20,
    },
    '40': {
      name: 'fortyHours',
      displayName: 40,
    }
  }
};

export {fields, colors};