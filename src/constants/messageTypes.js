export default {
  error: 'error',
  loading: 'loading',
  extensionOpen: 'extensionOpen',
  request: 'request',
  refresh: 'refresh',
}