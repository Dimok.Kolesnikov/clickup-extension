import DomController from "./dom/DomController";
import MessageListener from "./MessageListener";
import RequestMessage from "../message/messagesToSend/RequestMessage";
import RefreshMessage from "../message/messagesToSend/RefreshMessage";
import BaseMessage from "../message/messagesToSend/BaseMessage";
import destination from "../constants/destination";
import requestTypes from "../constants/requestTypes";
import ErrorMessage from "../message/messagesToSend/ErrorMessage";

window.onload = function () {
  const container = document.getElementById('mainContainer');
  container.style.display = 'block';
}

function assignOnClickToButtons() {
  const requestImportMessage = new RequestMessage(
    destination.contentScript,
    {requestType: requestTypes.importCSV}
  );
  const requestImportExampleMessage = new RequestMessage(
    destination.contentScript,
    {requestType: requestTypes.importCSVExample}
  );
  const requestExportMessage = new RequestMessage(
    destination.contentScript,
    {requestType: requestTypes.exportCSV},
  );
  const deleteErrors = new ErrorMessage(destination.contentScript, {errors: 'all', action: 'delete'});
  const refreshMessage = new RefreshMessage(destination.contentScript);
  domController.buttons.importCSVButton.assignOnClick(requestImportMessage.sendMessage);
  domController.buttons.importCSVExampleButton.assignOnClick(requestImportExampleMessage.sendMessage);
  domController.buttons.exportCSVButton.assignOnClick(requestExportMessage.sendMessage);
  domController.buttons.refreshButton.assignOnClick(refreshMessage.sendMessage);
  domController.errorInput.deleteButton.assignOnClick(deleteErrors.sendMessage);
}

function startMessageListener() {
  const messageListener = new MessageListener(domController);
  messageListener.startListen();
}

function sendExtensionOpenMessage() {
  const baseMessage = new BaseMessage(destination.contentScript, domController);
  baseMessage.extensionOpen();
}

const domController = new DomController();
assignOnClickToButtons();
startMessageListener();
sendExtensionOpenMessage();



