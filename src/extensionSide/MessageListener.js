import messageTypes from "../constants/messageTypes";

class MessageListener {
  constructor(domController) {
    this.domController = domController;
    this._listenerHandler = this._listenerHandler.bind(this);
  }

  startListen() {
    chrome.runtime.onMessage.addListener(this._listenerHandler);
  }

  _listenerHandler(request, sender, sendResponse) {
    this.domController.disableButtons(false);
    switch (request.type) {
      case messageTypes.loading:
        this._loadingHandler(request, sender, sendResponse);
        break;
      case messageTypes.error:
        this._errorsHandler(request, sender, sendResponse);
        break;
      default:
        console.error('Unknown request', request);
    }
  }

  _loadingHandler(request) {
    const {isLoading, message, isRefresh} = request;
    if (isLoading) {
      this.domController.startLoader(isRefresh, true);
    } else {
      this.domController.startLoader(isRefresh, false);
    }
  }

  _errorsHandler(request) {
    const {errors} = request;
    const {errorInput} = this.domController;
    errorInput.createErrors(errors);
  }
}

export default MessageListener;