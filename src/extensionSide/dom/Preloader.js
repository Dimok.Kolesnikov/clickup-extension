class Preloader {
  constructor(id) {
    this.preloader = null;
    this._init(id);
  }

  _init(id) {
    const preloaderContainer = document.createElement('div');
    const preloader = document.createElement('div');
    preloaderContainer.id = 'preloaderContainer'
    preloader.classList.add('preloader');
    preloaderContainer.appendChild(preloader);
    this.preloader = preloaderContainer;
  }

  startPreloader(message) {
    const preloader = document.getElementById('preloaderContainer');
    if (!preloader) {
      const mainContainer = document.getElementById('mainContainer');
      mainContainer.appendChild(this.preloader);
    }
  }

  stopPreloader() {
    const preloader = document.getElementById('preloaderContainer');
    if (preloader) {
      const mainContainer = document.getElementById('mainContainer');
      mainContainer.removeChild(preloader);
    }
  }
}

export default Preloader;