class Button {
  constructor(id, handlers) {
    this.button = null;
    this.label = null;
    this._init(id, handlers);
  }

  _init(id, handlers) {
    const button = document.getElementById(id);
    if (id === 'exportCSV') {
      this.label = document.getElementById('exportCSVLabel');
    }
    if (button) {
      this.button = button;
      if (handlers) {
        Object.entries(handlers).forEach(([methodName, methodBody]) => {
          if (this[methodName]) {
            this[methodName](methodBody);
          }
        });
      }
    } else {
      throw new Error(`Button with id ${id} not found`);
    }
  }

  addClass(className) {
    this.button.classList.add(className);
  }

  removeClass(className) {
    this.button.classList.remove(className);
  }

  disable(value) {
    if (this.label) {
      if (value) {
        this.label.classList.add('disabled');
      } else {
        this.label.classList.remove('disabled');
      }
    }
    this.button.disabled = value;
  }

  assignOnClick(method) {
    this.button.onclick = method;
  }

  assignOnChange(method) {
    this.button.onchange = method;
  }
}

export default Button;