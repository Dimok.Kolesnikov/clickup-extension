import Button from "./Button";
import Preloader from "./Preloader";
import ErrorInput from "./ErrorInput";

class DomController {
  constructor() {
    this.buttons = {};
    this.preloader = null;
    this.errorInput = null;
    this.body = document.body;
    this.ids = {
      button: {
        importCSV: 'importCSV',
        importExample: 'importExample',
        exportCSV: 'exportCSV',
        refresh: 'refresh',
      },
      preloader: 'preloader',
      errorInput: 'errorInputContainer',
    }
    this._init();
  }

  _init() {
    this._initButtons();
    this._initPreloader();
    this._initErrorInput();
  }

  _initErrorInput() {
    this.errorInput = new ErrorInput(this.ids.errorInput);
  }

  _initPreloader() {
    this.preloader = new Preloader(this.ids.preloader);
  }

  _initButtons() {
    const importCSVButton = new Button(this.ids.button.importCSV);
    const exportCSVButton = new Button(this.ids.button.exportCSV);
    const refreshButton = new Button(this.ids.button.refresh);
    const importCSVExampleButton = new Button(this.ids.button.importExample);
    this.buttons = {importCSVButton, exportCSVButton, refreshButton, importCSVExampleButton};
  }

  startLoader (isRefresh, value) {
    if (value) {
      if (isRefresh) {
        this.buttons.refreshButton.addClass('loading');
        this.disableButtons(value);
      } else {
        this.preloader.startPreloader();
      }
    } else {
      if (isRefresh) {
        this.buttons.refreshButton.removeClass('loading');
        this.disableButtons(value);
      } else {
        this.preloader.stopPreloader();
      }
    }
  }

  disableButtons (value) {
    Object.values(this.buttons).map(button => {
      button.disable(value);
    });
    this.errorInput.deleteButton.disable(value);
  }
}

export default DomController;