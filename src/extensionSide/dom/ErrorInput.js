import Button from "./Button";
import errorTypes from "../../constants/errorTypes";

class ErrorInput {
  constructor(id) {
    this.errorInput = null;
    this.errorListContainer = null;
    this.deleteButton = null;
    this._init(id);
  }

  _init(id) {
    const errorInput = document.getElementById(id);
    const errorListContainer = document.getElementById('errorListContainer');
    this.deleteButton = new Button('deleteButton');
    if (errorInput && errorListContainer) {
      this.errorInput = errorInput;
      this.errorListContainer = errorListContainer;
    } else {
      throw new Error(`ErrorInput with id ${id} not found`);
    }
  }

  multipleErrorTemplate(messages) {
    let rowNumber = null;
    let messagesTest = [];
    const appendError = () => {
      const error = this.errorTemplate(messagesTest, `Строка #${rowNumber}`);
      this._appendToErrorListContainer(error);
    }
    messages.forEach(message => {
      const {text, errorNumber} = message;
      if (rowNumber === null) {
        rowNumber = errorNumber;
        messagesTest.push(text);
      } else {
        if (errorNumber !== rowNumber) {
          appendError();
          rowNumber = errorNumber;
          messagesTest = [];
          messagesTest.push(text);
        } else {
          messagesTest.push(text);
        }
      }
    });
    appendError();
  }

  errorTemplate(text, title) {
    const error = document.createElement('div');
    const errorTitle = document.createElement('div');
    const errorText = document.createElement('div');
    errorTitle.classList.add('title');
    errorText.classList.add('text');
    error.classList.add('error');
    errorTitle.innerText = title;
    error.appendChild(errorTitle);
    if (Array.isArray(text)) {
      text.forEach(item => {
        const point = document.createElement('div');
        point.classList.add('point');
        point.innerText = item;
        errorText.appendChild(point);
      });
    } else {
      errorText.innerText = text;
    }
    error.appendChild(errorText);
    return error;
  }

  errorsCategoryTemplate(type) {
    const errorCategory = document.createElement('div');
    errorCategory.classList.add('errorsCategory');
    errorCategory.innerText = errorTypes[type].title;
    return errorCategory;
  }

  createErrors(errors) {
    this.errorListContainer.innerHTML = null;
    if (errors) {
      let errorsCategory;
      let index = 0;
      errors.forEach(errorItem => {
        const {message, type} = errorItem;
        if (type !== errorsCategory) {
          errorsCategory = type;
          const errorCategory = this.errorsCategoryTemplate(type);
          this._appendToErrorListContainer(errorCategory);
          index = 0;
        }
        const title = `${errorTypes[type].errorTitle} #${index + 1}`;
        if (Array.isArray(message)) {
          this.multipleErrorTemplate(message);
        } else {
          const error = this.errorTemplate(message, title);
          this._appendToErrorListContainer(error);
        }
        index += 1;
      });
      this.errorInput.style.display = 'flex';
    } else {
      this.errorInput.style.display = 'none';
    }
  }

  _appendToErrorListContainer(element) {
    this.errorListContainer.appendChild(element);
  }
}

export default ErrorInput;