const Dotenv = require('dotenv-webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
const isDevelopment = NODE_ENV === 'development';
const availableModes = {
  production: 'production',
  development: 'development',
  none: 'none',
}

module.exports = {
  entry: {
    contentScript: './src/contentSide/main.js',
    extensionScript: './src/extensionSide/main.js'
  },
  output: {
    path: __dirname + '/dist',
    filename: "[name].js"
  },
  mode: availableModes[NODE_ENV] || 'development',
  plugins: [
    new Dotenv(),
  ],
  devtool: 'cheap-module-source-map',
  stats: {
    errorDetails: isDevelopment,
  },
  watch: isDevelopment,
};